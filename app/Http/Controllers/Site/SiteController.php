<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class SiteController extends Controller
{
    
	/*public function __construct () {

		$this -> middleware ('auth')
			->only ([
				'contatos',
				'categoria'

			]);


	}*/


    public function index()
    {

    	return view('welcome');

    }

    public function contatos()

    {

    	return view('contatos');

    }
}
