<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});
*/


Route::group(['namespace' => 'Site'], function(){

Route::get('/contatos', 'SiteController@contatos');
Route::get('/', 'SiteController@index');


});

Route::group(['namespace' => 'Painel'], function(){

Route::get('/painel', 'PainelController@index');

});

Route::group(['namespace' => '\Painel\Caixa'], function(){

    Route::get('/painel/caixa', 'CaixaController@index');
    Route::get('/painel/caixa/create', 'CaixaController@form');

});


